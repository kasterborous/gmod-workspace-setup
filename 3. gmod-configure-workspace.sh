#! /bin/bash

error()
{
    echo $*
    sleep 3
    exit 1
}

which code &>/dev/null
[[ $? != 0 ]] && error "ERROR: Failed to find VS Code"

which git &>/dev/null
[[ $? != 0 ]] && error "ERROR: Failed to find Git"

[[ -d ./scripts ]] || error "ERROR: installer files not found"

./scripts/gitconfig.sh
./scripts/vscode_extensions.sh
./scripts/ssh_config.sh

if [[ ! -f "$APPDATA/Code/User/settings.json" ]]
then
    cat ./data/vscode-settings-gmod.json >"$APPDATA/Code/User/settings.json"
fi

bash
exit 0