# GMod workspace setup (git & vs code)

## What is this, and why do I need it?

This is the simplified workspace setup for working on GMod projects.  
It includes files which will help you install and configure git and vs code on Windows.  

**VS Code** is a very convenient text editor, which can be configured for easier work with git and gmod's specific lua & vmt files.

**Git** is a version control system.  
Git is a simple tool used in practically all software development, whether it's google, microsoft, minecraft or gmod / s&box.  

***

# How do I use git?

see [here](git-guide-vscode.md)

***

# How to use the setup?

## Download the project

Open the project page on gitlab and click the "Download" button

![](/img/1/01.png)

Select the .zip format

![](/img/1/02.png)

Extract the `gmod-workspace-setup-master.zip` archive into a folder

After the project is extracted, your folder should contain these files:

![](/img/1/03.png)

***

## Download VS Code

For your convenience, the project contains the official links of VS Code and Git installers.

Double click the file `1. Download VS Code` to get the latest version of VS Code:  

![](/img/1/04.png)

The download should start automatically

![](/img/1/05.png)

Alternatively, you could just get the latest version from:  
https://code.visualstudio.com/Download

***

## Download Git

Click the second file `2. Git Download link` in the project

![](/img/1/06.png)

The page with git downloads will open.  
Click the very first download link:

![](/img/1/07.png)

You can also open the git download page directly from here:  
https://git-scm.com/download/win

***

## Install VS Code

Install vs code with the use of downloaded installer:

![](/img/1/08.png)

**IMPORTANT!!!**

You need to make sure that these parameters are enabled during the installation:

![](/img/1/09.png)

The first two options enable very convenient options for your file explorer, which allow you to quickly open whole projects as folders.  
If you previously had an installation of VS Code without these options active, I strongly recommend you to reinstall the editor with them (your settings and data will be kept).

***

## Install Git

Open the second installer to install Git on your computer:

![](/img/1/10.png)

This installation will ask you to specify MANY settings (17+ steps).  
You can leave all of them to being default values and skip them by pressing "Next".  
Those options will be configured automatically by the scripts.

***

## Running the configuration script

Open the third file `3. gmod-configure-workspace.sh` :

![](/img/1/11.png)

Make sure the file is associated with "Git for Windows" app.  
If it isn't, you need to select it manually:

![](/img/1/12.png)

It might be convenient to permanently associate all .sh files with Git Bash:

![](/img/1/13.png)
![](/img/1/14.png)

> **Note**: if after opening the script you get the "installer files not found" error, try closing the window and reopening it with a double-click.  
>
> If the error keeps happening, please contact me in any convenient way.  
  

After running the script, the following window should open:

![](/img/1/15.png)

Enter your email and nickname which you want to be used with git.  
If you later try to contribute to any of the projects, these will be used to sign your changes.  

![](/img/1/16.png)

The script will then configure git and vs code, as well as install the vs code extensions you need.

**!!!**  
***DO NOT CLOSE THE WINDOW* AFTER THE SCRIPT FINISHES RUNNING**  
**!!!**  

The result of the script should look like this:

![](/img/1/17.png)

> In case you already had VS Code installed and configured before running this setup, VS Code settings will not be changed.
>
> This case is described lower in the ["Setting up VS Code settings manually"](#setting-up-vs-code-settings-manually) section

Before closing the script, you need to **copy the SSH key**.  
The key is the text starting with `ssh-rsa` and ending at the bottom of the console window:

![](/img/1/18.png)

Most likely, this console window will not support the `ctrl+C` key binding, so you'll have to use right click menu instead:

![](/img/1/18-2.png)

***

## Setting up GitLab

Go back to gitlab.com  
Make sure you are logged in.

Click on your profile picture and click "Preferences":

![](/img/1/19.png)

Open the "SSH Keys" menu on the side bar:

![](/img/1/20.png)
![](/img/1/21.png)

Paste the copied SSH key into the form and click "Add key".  
"Title" and "Expiration date" will be filled automatically:

![](/img/1/22.png)

If everything is okay, you will end up with an added SSH key looking like this:

![](/img/1/23.png)

***

## Setting up GitHub

If you're planning to work with Git***Hub*** projects as well as Git***Lab*** ones, you need to add the SSH key to github.com as well.  

*You may skip this step otherwise.*


> Note: GitLab and GitHub are different web sites to host git projects.
> In case of GMod TARDISes, the **main (core) addon** is created on git*HUB* while most of **the extension** projects mostly use GitLab.
>
> You can use the gitHUB account to sign into gitLAB.  


Adding the SSH key to github is very similar to gitlab.

After loggin in, click the profile picture and open "Settings":

![](/img/1/24.png)

Open the "SSH and GPG keys" menu and click "New SSH key":

![](/img/1/25.png)
![](/img/1/26.png)

Paste the key and type in any title, then click "Add SSH key":

![](/img/1/27.png)

### Congratulations - you have completed the installation!

\*  
\*  
\*  
\*  
\*  
***
## Setting up VS Code settings manually

> Note: This section is only required for those who had VS Code installed and set up before running this setup.
>
> If your VS Code settings have been applied automatically by the script, you don't need to do any of this.

To change VS Code settings, open VS Code and press `ctrl + shift + P`.  
A small window will open at the top of the screen:

![](/img/1/28.png)

Type in "JSON" and find the option called `Preferences: Open User Settings`

![](/img/1/29.png)

> Make sure you select "User Settings", not "Default Settings" or "Workspace Settings".

***

Clicking that should open a json settings file which will look something like this:

![](/img/1/30.png)

To add the parameters, simply add the lines from below to the json file.  

> Make sure all options are separated by commas:
> 
> ![](/img/1/31.png)
> 
> ![](/img/1/32.png)

***
### Required settings:
(best if copied in all cases unless you have the experience)
```json
    "git.pruneOnFetch": true,
    "git-graph.dialog.fetchRemote.prune": true,
    "git-graph.dialog.fetchRemote.pruneTags": true,
    "git-graph.repository.fetchAndPruneTags": true,
    "git-graph.repository.fetchAndPrune": true,

    "git.showUnpublishedCommitsButton": "never",
    "git.suggestSmartCommit": false,
    "git.showActionButton": {
        "commit": false,
        "publish": false,
        "sync": false
    },

    "terminal.integrated.defaultProfile.windows": "Git Bash",
```
***
### Recommended / convenient settings:
(add the ones you want to the file)

### **All in one block:**

```json
    "editor.renderWhitespace": "all",
    "editor.rulers": [ 80, 100, 120, 150 ],
    "editor.detectIndentation": true,

    "security.workspace.trust.untrustedFiles": "open",
    "security.workspace.trust.banner": "never",
    "security.workspace.trust.startupPrompt": "never",
    "security.workspace.trust.enabled": false,

    "window.newWindowDimensions": "maximized",
    "window.restoreWindows": "none",
    "window.openFoldersInNewWindow": "on",
    "workbench.startupEditor": "newUntitledFile",

    "editor.tabCompletion": "on",
    "editor.stickyTabStops": true,

    "workbench.iconTheme": "vscode-icons",
    "workbench.colorTheme": "Tomorrow Night Blue",
```

### **Details:**

Make the editor show the space symbols in code:
```json
    "editor.renderWhitespace": "all",
```

Add rulers in the code for better alignment & readability:
```json
    "editor.rulers": [ 80, 100, 120, 150 ],
```

Make the editor automatically detect the indentation of the file:
```json
    "editor.detectIndentation": true,
```

Remove the annoying "trust" banners while opening VS Code:
```json
    "security.workspace.trust.untrustedFiles": "open",
    "security.workspace.trust.banner": "never",
    "security.workspace.trust.startupPrompt": "never",
    "security.workspace.trust.enabled": false,
```

Open VS Code maximized with an empty file by default:
```json
    "window.newWindowDimensions": "maximized",
    "window.restoreWindows": "none",
    "window.openFoldersInNewWindow": "on",
    "workbench.startupEditor": "newUntitledFile",
```

Let the editor help you while writing code:
```json
    "editor.tabCompletion": "on",
    "editor.stickyTabStops": true,
```

Adds nice folder icons:
```json
    "workbench.iconTheme": "vscode-icons",
```

A nice blue color theme for VS Code:
```json
    "workbench.colorTheme": "Tomorrow Night Blue",
```

