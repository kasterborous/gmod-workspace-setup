# A guide about using **git** (in VS Code)

It is presumed that you already have the workspace for git fully prepared.
If you don't, please check [the workspace setup](README.md) first.

**Contents:**
- [A guide about using **git** (in VS Code)](#a-guide-about-using-git-in-vs-code)
	- [Why do I need git?](#why-do-i-need-git)
	- [Do not confuse git, GitHub and GitLab](#do-not-confuse-git-github-and-gitlab)
	- [How does git work?](#how-does-git-work)
		- [**Commits**](#commits)
		- [**Branches**](#branches)
	- [How to get a git project](#how-to-get-a-git-project)
	- [Opening the project](#opening-the-project)
	- [Git Graph (the main git interface)](#git-graph-the-main-git-interface)
	- [Switching between branches or commits](#switching-between-branches-or-commits)
	- [Updating the project: fetching changes and pulling the branches](#updating-the-project-fetching-changes-and-pulling-the-branches)
		- [To **update your branch**, you need to:](#to-update-your-branch-you-need-to)
		- [Alternative way to **update your branch**:](#alternative-way-to-update-your-branch)
	- [Making changes to a project and creating new commits](#making-changes-to-a-project-and-creating-new-commits)
	- [Merging branches and changes](#merging-branches-and-changes)
	- [Starting your own git project](#starting-your-own-git-project)

***

## Why do I need git?

**Git** is a simple tool used in practically all software development, whether it's google, microsoft, minecraft or gmod / s&box.  

Using git will allow you to:
* Collaborate on your projects with others, share your changes with people you select
* Save 99% of internet traffic while sharing versions of your projects
* Have secure backups of your projects
* Get and quickly update other projects featured in kasterborous gitlab server
* Get the work in progress versions of the addons, test the previews of the new features
* Suggest your own changes and features to other projects
* Protect your author rights in the projects you've worked on


***
## Do not confuse git, GitHub and GitLab

**git** is a program / tool / utility used for working on your projects

**GitHub** is `github.com`, a website for storing and hosting the projects made with git.

**GitLab** is another website for the same purpose (`gitlab.com`)

While GitHub is older and more popular, GitLab is more convenient for working in teams.


***

## How does git work?

Imagine you have a huge project, and you want to work on it with a friend.  
- If you both make some changes and he sends them over to you, two projects will be wasting space on your hard drive.  
- You will then have to look through all of his changes to make one addon after two.  
- Lots of time and internet traffic will be spent uploading and downloading the projects.

![meme](img/2/00.png)

- Instead of storing multiple versions of your project, **Git** saves the lists of **differences** between them, and allows to share those.  
- If you only changed one line in one file, git will save that exact line instead of the whole thing.  
- By knowing the differences between all versions of the project, git allows you to see and work on any version you want. That's why it's called a `version control system`.

### **Commits**

A single saved modification of your project is called **a commit**.  
Commits can consist of several files changed.

Each commit has a message (a description of the change) + information about its author & creation date.

When you work on a git project, commits (= saved changes) follow each other as a chain:

![](img/2/05.png)

At any time while working on your project, you can switch between commits or revert any commited changes. That literally allows you to time travel: you can now see the project at any point of its history.

Here's an example of a commit:
![](img/2/100.png)

Here's an example of change in the commit:
![](img/2/101.png)

### **Branches**

The history of the project is not always one line. Sometimes, people work on several changes at once.  
Then, the history of the project splits into separate **branches**:

![](img/2/06.png)

**A branch can be created from any commit.**  
People working on different branches do not mess with each other's work.  

Afterwards, branches are usually **merged** into one another.  
In that case, git combines the changes from both branches automatically.  
If that can't be done, git asks the user to resolve the merge conflict.

> Usually each long-lasting project has a branch for its stable version.  
> (sometimes called `master`, `main`, `stable` or `dev`)  
>
> - When a developer wants to make a change, he branches off the stable branch.  
> - He works on his own branch until it's ready.  
> - When the change is ready, its branch is merged into the stable one.  
>
> Here's an example of such project organization:  
> ![](img/2/07.png)


***
## How to get a git project

1. Open the project page

2. Click "Clone" and copy the SSH address
  ![](img/2/09.png)

3. Open VS Code

4. Press `CTRL + SHIFT + P` and type in "clone"

5. Find and select the `Git: Clone` option (in the small window at the top):  
  ![](img/2/10.png)

6. Paste the address into the opened interface and select "Clone from URL)  
  ![](img/2/12.png)  
  ![](img/2/11.png)  

7. Select the folder in which the project folder should go  
  (in case of gmod, it should be the `addons` folder)  
  ![](img/2/13.png)

8. Wait for the cloning process to end  
  ![](img/2/14.png)

9. The download should end with the following question:  
  ![](img/2/15.png)  
  You can click "open in new window" to open the project

***

## Opening the project

In order to work with git, you need to open the project as a folder (not just separate files).

If you've installed VS Code the correct way, all you have to do is right click the folder of the project and click "Open with Code":

  ![](img/2/16.png)

> If you **don't have that option** in the menu, you could either:
> * [reinstall VS Code](README.md#install-vs-code)  without deleting it  
>	By doing that, you would be able to add those options to your explorer and keep your settings an data
> OR
> * use the "File" menu in VS Code (not very convenient):  
>	![](img/2/17.png)
***
## Git Graph (the main git interface)

Once you have the project opened, in order to work with git, you need to open the "Source control" panel on the left and click the git graph icon:

  ![](img/2/18.png)
  ![](img/2/19.png)

A graph similar to this one should open:

  ![](img/2/20.png)

Each line represents a ***commit*** (a saved set of changes)  
The dots on the graph (on the left) show the way commits follow each other in branches

  ![](img/2/21.png)

You can see the detailed information about the changes of the commit by clicking the line with it. By clicking the files on the right, you can browse the changes made in them.
> yellow files are modified, green ones are the new ones, the red one is deleted

  ![](img/2/22.png)

The rectangles to the left from the commits are ***branches***

  ![](img/2/23.png)

There are remote and local branches.  
* Remote branches are the branches on gitlab / github.
* Local branch is the one on your computer.

* Remote branch has the `origin/` prefix in front of its name  
  > *Example:* `origin/poogie_catwalks` and `origin/poogie_texture_dev`

* Local branches don't have a prefix 
  > *Example:* `poogie_sound_dev`

* When the remote branch matches the local one, the `origin` text is written to the right from it.
  > *Example:* `dev` (the local "dev" matches gitlab's "dev")

* The current branch you are on is highlighted in bold and has a circle next to it  
(it's `poogie_sound_dev` in the screenshot)

***
## Switching between branches or commits

When you switch between branches / commits, the files in your folder **automatically change** to their state at the commit your branch is on.  
Example:

  ![](img/2/33.png)

  ![](img/2/34.png)

* To **switch between branches**, you can just *double-click* on a branch.

  Alternatively, you can *right-click* the branch and click "checkout branch"  
  ![](img/2/24.png)

* To **create a new branch**, you just *right click* a commit and click "create branch"

  ![](img/2/27.png)

  It is best to name your branches in lowercase letters and split words with "_".
  Use this tick to checkout at the branch you're creating:

  ![](img/2/28.png)

  Done:  
  ![](img/2/29.png)

* You cannot checkout at the remote (`origin/...`) branches. Instead, by trying to checkout a remote branch, you create the same one locally:

  ![](img/2/30.png)
  ![](img/2/31.png)

  Result:

  ![](img/2/32.png)

* In theory, you can checkout at any commit even if it doesn't have a branch on it:  
  ![](img/2/25.png)

  When you're checked out on **a commit** instead of **a branch**, you can not make new commits / changes to the project. It looks like this:  
  ![](img/2/26.png)

  The solution to this would be simply ***creating a new branch*** at the commit you're on.

***
## Updating the project: fetching changes and pulling the branches

To get the project's latest changes, you need to click the fetch button:

![](img/2/35.png)

There is a chance that the branch you had locally has new commits on it.  
This is how it looks:

![](img/2/36.png)

Basically, in the screenshot the *remote branch* `origin/branch_example` (fetched from gitlab or github) is ahead of your local branch `branch_example`.

### To **update your branch**, you need to:

1. Check out at your local branch

2. Right click the remote (`origin/...`) branch and select "Pull into current branch"

	![](img/2/37.png)

3. Click "Yes, pull"

	![](img/2/38.png)

4. Done:  
	![](img/2/39.png)

### Alternative way to **update your branch**:

1. Try to checkout at the remote branch (double click or right click menu option)

	![](img/2/40.png)

2. Keep the default branch name and press "Checkout Branch":

	![](img/2/41.png)

3. In the opened window, select "Checkout the existing branch & pull changes"

	![](img/2/42.png)

4. That's it, the branch is updated!

	![](img/2/43.png)


***
## Making changes to a project and creating new commits

1. **Create a branch you want to make changes on (if you don't have one already).**

	![](img/2/44.png)

	It's best to name the branches after the features you're working on.
	In projects you work with many others on, it's also a good habit to begin your personal branch name with your nickname to make sure the branch name is unique.

	Done:  

	![](img/2/45.png)

	\***

2. **Browsing the changes**

	Let's say you've made some changes to the projects:

	![](img/2/46.png)

	If you open the `Source Control` panel on the left, you'll see that you can browse the changes you've made:

	![](img/2/47.png)

	Clicking on those files will open that file's editor in the comparison mode:  
	(to show all your changes from the latest commit)

	![](img/2/48.png)

	In front of each file, there is also an "open file" icon which allows you to open it in normal (non-comparison) mode:

	![](img/2/49.png)

	\***

3. **Discarding the changes**

	You can reset the changes made to one of the files by clicking the "back arrow" in front of the file:  
	![](img/2/50.png)

	You can click the similar button at the top of the change list to reset **ALL** of the files in the folder:  

	![](img/2/51.png)

	**CAREFUL!** Resetting files like that can NOT be reversed!!!

	\***

4. **Adding files to the commit**

	Before making the commit, you need to select the changes you want to save in it.  
	Selecting those files is called "*staging the file*" or "*adding the file to index*".

	To do that, click `"+"` button on the file  

	![](img/2/52.png)


	The files you've added to the commit will be shown at the top of the panel in the `Staged Changes` section. The files in the `Changes` section will not be added to the commit.

	![](img/2/53.png)

	You can remove the staged files from index by clicking `"-"` near them:

	![](img/2/54.png)

	If you select multiple files, clicking "+" or "-" will apply to all of them.

	Just like in case of discarding the changes, you can click one button to add / remove all files from index:

	![](img/2/55.png)  
	![](img/2/56.png)  

5. **Creating the commit**

	Before creating a commit, you need to specify a commit message (a description of what the change is):

	![](img/2/57.png)

	After that, press `CTRL + ENTER` or simply click the tick on the panel above:

	![](img/2/58.png)

	Done! As you can see by the graph, a new commit has been created with the changes you've selected:

	![](img/2/59.png)

6. **Uploading changes / Pushing a branch**

	When you have made the changes, you only have them on your computer at local branches.  
	In order to upload a branch to a github / gitlab project, you need to right click the branch and select "Push Branch":

	![](img/2/60.png)

	Click "Yes, push" in the opened window.  

	**IMPORTANT!!! *NEVER SELECT "FORCE" OPTION WHILE PUSHING A BRANCH !!!***  
	That's a potential project-breaking action.  

	As long as you **do not do force pushes** and do **not delete remote branches**, you can not ruin an uploaded git project.

	![](img/2/61.png)

	In case you've made some more changes to the branch, your local branch will be ahead of the remote one:

	![](img/2/62.png)

	You can just update the remote branch by pushing the local one once again:

	![](img/2/63.png)

	As you can see, after pushing your branch, the remote and local versions of it will in sync with each other:

	![](img/2/64.png)

	Congratulations!  
	You have successfully learned to make changes to the git projects!

***
\*  
\*  
\*  
***

## Merging branches and changes

In case you have two branches you want to make into one (for example, the branches with two different features or made by different people), you need to **merge** them.  
To do that:

1. Switch to the branch you want to combine those branches on  
	In this example, I'm gonna be merging `parar_teleport_tweaks` into `2014_lights`, so I need to checkout at `2014_lights`

	![](img/2/65.png)


2. Right click the second branch and click `Merge into current branch`, then click `Yes, merge` in the opened dialogue:

	![](img/2/66.png)
	![](img/2/67.png)

3. If the merge can happen automatically, a new "merge" commit will appear on your branch:

	![](img/2/68.png)

	In that case, that will be the end of merging process

***

4. Sometimes, git can not merge the branches in automatic mode. That situation is called a **merge conflict.**  

	An example of a merge conflict would be when you change the same lines in different ways at different branches:

	> ![](img/2/69.png)
	> Adding a change to branch `example_a`

	> ![](img/2/70.png)
	> Adding a different change to the same place at branch `example_a`

	![](img/2/71.png)

	Let's try merging `example_a` into `example_b`:

	![](img/2/72.png)

	We get an error during the merge progress:  
	![](img/2/73.png)

	That means we need to **resolve the merge conflicts**.  
	The files with them will appear on the left panel in the new `Merge Changes` section:  
	![](img/2/74.png)

	The conflict itself should be shown in the editor like this:  
	![](img/2/75.png)

	Instead of the merged code, the file will have something like this:  
	```
	<<<<<<< ...
	version 1
	=======
	version 2
	>>>>>>> ...
	```

	You need to replace this paragraph with the right version of the code.  
	Sometimes you need to keep both changes, sometimes you need to select only one.  

	VS Code has an additional interface above the code, which allows you to select a change or compare the changes:

	![](img/2/77.png)  
	![](img/2/76.png)  

	After you've resolved the conflict(s) and saved the file, you need to add it to the index once again:

	![](img/2/78.png)

	After you've resolved all merge conflicts (**there can in theory be many files with them and many conflicts in one file**), you commit the changes like you usually do:

	![](img/2/79.png)

	The merge is complete!

	![](img/2/80.png)

***
## Starting your own git project

If you have a project that isn't linked to git yet, you can start using git to work on it.  
To do so:

1. Open the full project's folder in VS Code

	![](img/2/81.png)

	Go to the `Source Control` panel:

	![](img/2/82.png)

2. Click `Initialize Repository`:

	![](img/2/83.png)

3. Add all the changes to the index and make a commit:

	![](img/2/84.png)

	Usually, the first commit in the project is called "Initial commit":

	![](img/2/85.png)

4. After commiting the project, you can open Git Graph to make sure everything is okay:

	![](img/2/86.png)

At this point, you can make local branches and commits to manage versions of your project.  
If you only want to use git **locally** (on your computer), it's done and you **don't need anything else**.  

If you want to **link your project to github / gitlab** and push / pull your changes or collaborate with someone, you need to link it:

5. Create a github / gitlab project:

	![](img/2/87.png)  
	![](img/2/88.png)  

	The project creation is pretty similar at both web sites.  
	You can specify the privacy / visibility settings there  

	Make sure this two ticks are disabled:

	![](img/2/90.png)  

	**NOTE:** if you have the access to the kasterborous group on gitlab, you need to specify whether the project will be listed as your personal OR a part of the group:

	![](img/2/89.png)  

	> * If the gitlab project is **public**, it will be visible to everyone
	>
	> * If the gitlab project is **private** and listed **in kasterborous group**, it's only visible to kasterborous developers, testers and extensions owners, but not visible to public
	>
	> * If the gitlab project is **private** and listed **as your personal**, noone else can see it unless you add them to your project manually

6. After the project is created, you need to copy its SSH address (like you did [when you were cloning one](#how-to-get-a-git-project))

	![](img/2/91.png)
	![](img/2/92.png)  

7. Open the git graph of your local project and go to the "settings":

	![](img/2/93.png)  

	In the "Remote configuration" click `Add Remote`:

	![](img/2/94.png)  

	The name is usually "origin".  
	Paste the SSH address into the "Fetch URL" field:

	![](img/2/96.png)  

	After doing that, you can push your branch to the project:

	![](img/2/97.png)  

	**That's it!**  
	Your project is set up.  
	As you can see, the remote version of `master` branch (`origin/master`) has appeared, same as the "fetch" button (cloud icon) at the right top of the screen:  

	![](img/2/98.png)  

	After that, you can work with your project the same way as you would when you cloned it.

***
**That's all folks!**

This guide has been made by @parar020100 for the kasterborous project.  
Discord server of this project: https://discord.gg/sg9QDgN3cJ  
Any feedback and suggestions to this guide will be very welcome.  

***
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
Yes, the guide is over. You can stop scrolling now.  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
You've now scrolled down 100 lines of "*"  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
Stop it. It's pointless now.  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
Don't you have any better things to do?  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
Stop scrolling.  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
Stop it!!!  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
Whatever happens next, it's your fault.  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
\*  
We're no strangers to love  
You know the rules and so do I  
A full commitment's what I'm thinking of  
You wouldn't get this from any other guy  
I just wanna tell you how I'm feeling  
Gotta make you understand  
![](img/2/99.gif)  
Never gonna give you up  
Never gonna let you down  
Never gonna run around and desert you  
Never gonna make you cry  
Never gonna say goodbye  
Never gonna tell a lie and hurt you  
We've known each other for so long  
Your heart's been aching but you're too shy to say it  
Inside we both know what's been going on  
We know the game and we're gonna play it  
And if you ask me how I'm feeling  
Don't tell me you're too blind to see  
Never gonna give you up  
Never gonna let you down  
Never gonna run around and desert you  
Never gonna make you cry  
Never gonna say goodbye  
Never gonna tell a lie and hurt you  
Never gonna give you up  
Never gonna let you down  
Never gonna run around and desert you  
Never gonna make you cry  
Never gonna say goodbye  
Never gonna tell a lie and hurt you  
Never gonna give, never gonna give  
(Give you up)  
We've known each other for so long  
Your heart's been aching but you're too shy to say it  
Inside we both know what's been going on  
We know the game and we're gonna play it  
I just wanna tell you how I'm feeling  
Gotta make you understand  
Never gonna give you up  
Never gonna let you down  
Never gonna run around and desert you  
Never gonna make you cry  
Never gonna say goodbye  
Never gonna tell a lie and hurt you  
Never gonna give you up  
Never gonna let you down  
Never gonna run around and desert you  
Never gonna make you cry  
Never gonna say goodbye  
Never gonna tell a lie and hurt you  
Never gonna give you up  
Never gonna let you down  
Never gonna run around and desert you  
Never gonna make you cry  
Never gonna say goodbye  
  
