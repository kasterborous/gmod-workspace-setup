#! /bin/bash

git config --global core.editor "code --wait"
git config --global pull.ff only

echo -n "Please enter the email to use with git: "
read email
echo -n "Please enter the name / nickname to use with git: "
read name
git config --global user.name "$name"
git config --global user.email "$email"