#! /bin/bash

if [[ -f ~/.ssh/id_rsa || -f ~/.ssh/id_rsa.pub ]]
then
    echo "The SSH key already exists"
else
    echo "Generating the SSH key..."
    ssh-keygen -N '' -q -f ~/.ssh/id_rsa
fi

ssh-keyscan -t rsa -H github.com >>$HOME/.ssh/known_hosts
ssh-keyscan -t rsa -H gitlab.com >>$HOME/.ssh/known_hosts

echo -en "\n\nSSH Key:\n---\n"
cat ~/.ssh/id_rsa.pub

