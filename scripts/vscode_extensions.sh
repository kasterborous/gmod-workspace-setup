#! /bin/bash

code --install-extension FerrierBenjamin.fold-unfold-all-icone --force  \
--install-extension mhutchie.git-graph --force                          \
--install-extension eamodio.gitlens --force                             \
--install-extension venner.vscode-glua-enhanced --force                 \
--install-extension GEEKiDoS.vdf --force                                \
--install-extension VisualStudioExptTeam.vscodeintellicode --force      \
--install-extension shardulm94.trailing-spaces --force                  \
--install-extension vscode-icons-team.vscode-icons  --force
